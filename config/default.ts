export = {
  host: process.env.SERVER_HOST ?? 'localhost',
  port: process.env.SERVER_PORT ?? 3000,
  timeout: process.env.SERVER_TIMEOUT ?? 5000,
  jsonSizeLimit: process.env.SERVER_JSON_SIZE_LIMIT ?? '2mb',
  mongodb: {
    host: process.env.MONGODB_HOST ?? 'localhost',
    port: process.env.MONGODB_PORT ?? 27017,
    poolSize: process.env.MONGODB_POOL_SIZE ?? 5,
    serverSelectionTimeout:
      process.env.MONGODB_SERVER_SELECTION_TIMEOUT ?? 10000,
    replicaHosts: process.env.MONGODB_REPLICA_HOSTS ?? '',
    database: 'MONGODB_DATABASE',
    user: 'MONGODB_USERNAME',
    pass: 'MONGODB_PASSWORD',
  },
  candidateAnswers: {
    notAnsweredText: process.env.CANDIDATE_ANSWER_NOT_ANSWERED_TEXT ?? 'NA',
  },
  logger: {
    level: process.env.LOGGER_LEVEL ?? 'info',
  },
};
