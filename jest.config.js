module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  globals: {
    'ts-jest': {
      diagnostics: false,
    },
  },
  testPathIgnorePatterns: ['/node_modules/', '<rootDir>/config/', '/.build/'],
  coveragePathIgnorePatterns: ['/test/', '/config/'],
  setupFilesAfterEnv: ['jest-extended'],
};
