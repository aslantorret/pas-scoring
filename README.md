# pas-scoring

> Scoring service for PAS

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have
   [Docker](https://docs.docker.com/install/),
   [Docker Compose](https://docs.docker.com/compose/install/),
   [NodeJS](https://nodejs.org/),
   [R](https://cran.r-project.org/)
   and
   [yarn](https://yarnpkg.com/)
   installed.

2. Go to project root folder

   ```
   cd path/to/pas-scoring
   ```

3. Prepare docker-compose **.env** file

   ```
   cp .env.dist .env
   ```
   and populate all variables inside **.env** with appropriate values

4. Run docker-compose

   ```
   docker-compose up -d
   ```

5. Install project dependencies

   ```
   yarn install
   ```

6. Start your app

   ```
   yarn dev:start
   ```

## ENV
```
VOLUMES_PATH=.volumes
PAS_SCORING_IMAGE_NAME=pas-scoring

SERVER_HOST=localhost
SERVER_PORT=3003
SERVER_TIMEOUT=5000
SERVER_JSON_SIZE_LIMIT=7mb

LOGGER_LEVEL=info

MONGODB_HOST=192.168.100.3
MONGODB_PORT=27017
MONGODB_2_PORT=27018
MONGODB_3_PORT=27019
MONGODB_USERNAME=user
MONGODB_PASSWORD=pass
MONGODB_DATABASE=pas
MONGODB_POOL_SIZE=5
MONGODB_SERVER_SELECTION_TIMEOUT=10000

```
## Testing

Simply run `yarn dev:test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ yarn add -g @feathersjs/cli             # Install Feathers CLI

$ feathers g service               # Generate a new Service
$ feathers g hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).
