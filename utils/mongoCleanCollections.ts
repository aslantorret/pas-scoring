import { connection } from '../src/db';
import { logger } from '../src/log';

async function _mongoCleanCollections(): Promise<void> {
  // we need this trick, because mongooseClient.connection is a Promise<pending>
  await Promise.resolve(connection);
  if (connection && connection.readyState !== 1) {
    throw new Error(
      `mongoDb connection has wrong readyState value=${connection?.readyState}`,
    );
  }

  const { db } = connection;

  const collections = await db
    .listCollections({}, { nameOnly: true })
    .toArray();

  const cleaned: string[] = [];
  await Promise.all(
    collections.map(({ name }: { name: string }) => {
      cleaned.push(name);
      return db.collection(name).deleteMany({});
    }),
  );

  logger.info(`Successfully were cleaned collections: ${cleaned.join(', ')}`);
}

_mongoCleanCollections()
  .then()
  .catch((err) => {
    logger.error(err);
  })
  .finally(() => {
    connection.close();
  });
