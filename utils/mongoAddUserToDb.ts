import { Connection } from 'mongoose';
import { getConfig } from '../src/configuration';
import { initMongoose, MongodbConfig } from '../src/mongoose';
import { logger } from '../src/log';

let connection: Connection;

async function mongoAddUserToDb(): Promise<void> {
  const role = 'readWrite';
  const config: MongodbConfig = getConfig().mongodb;
  ({ connection } = initMongoose({
    ...config,
    user: 'root',
    database: 'admin',
  }));

  await Promise.resolve(connection);

  if (connection && connection.readyState !== 1) {
    throw new Error(
      `mongoDb connection has wrong readyState value=${connection?.readyState}`,
    );
  }

  const newCon = connection.useDb(config.database);
  const { db } = newCon;
  type usersInfo = {
    users: [
      {
        user: string;
        db: string;
        roles: [];
      },
    ];
  };
  const { users } = (await db.command({ usersInfo: 1 })) as usersInfo;
  const obj = users.find((el) => el.user === config.user);
  if (!obj) {
    await db.addUser(config.user, config.pass, {
      roles: [{ role, db: config.database }],
    });
    logger.info(
      `User [${config.user}] with role [${role}] for db [${config.database}] was successfully created`,
    );
    return;
  }

  logger.warn(
    `User [${obj.user}] with roles [${JSON.stringify(obj.roles)}] in db [${
      config.database
    }] already exists`,
  );
}

mongoAddUserToDb()
  .then()
  .catch((err) => {
    logger.error(err);
  })
  .finally(() => {
    connection.close();
  });
