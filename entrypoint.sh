#!/bin/sh

if [[ -z $MONGODB_HOST ]]; then
    echo "\$MONGODB_HOST is unset. Cannot start..."
    exit 1
fi

TIMEOUT=true
for i in {1..60}; do
    # Split the MONDODB_HOST by ,
    for uri in $(echo $MONGODB_HOST | tr ',' '\n'); do
        # Split host and port (if present)
        host=$(echo $uri | cut -d':' -f1)
        port=$(echo $uri: | cut -d':' -f2) # Ensure there is always something to cut in case port is empty
        port=${port:-27017} # Set port to 27017 if empty

        nc -z $host ${port} > /dev/null 2>&1
        result=$?

        if [[ $result -eq 0 ]]; then
            TIMEOUT=false
            break 2 # Break both loops
        else
            echo "Unable to connect to mongo db: \"$host\" on port \"${port}\". Retrying..."
            sleep 5
        fi
    done
done

if [[ "$TIMEOUT" = true ]]; then
    echo "Timeout connecting to mongo db. Exiting..."
    exit 1
fi

exec yarn start
