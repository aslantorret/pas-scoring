declare module '@feathers-plus/validate-joi' {
  import Joi from '@hapi/joi';

  function form(
    schema: Joi.ObjectSchema,
    options?: object,
    translations?: object,
    ifTest?: boolean,
  ): Promise;
  function mongoose(
    schema: Joi.ObjectSchem,
    options?: object,
    translations?: object,
    ifTest?: boolean,
  ): Promise;
  export = { form, mongoose };
}
