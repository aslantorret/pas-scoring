# Base image
FROM node:13-alpine as base

# Upgrade to Edge to get R 4.0+ according to https://wiki.alpinelinux.org/wiki/Upgrading_Alpine
RUN sed -i -e 's/v[[:digit:]]\..*\//edge\//g' /etc/apk/repositories && \
    apk --no-cache upgrade

# Install R and dependencies
# Because readxl haven't been released since they fixed their alpine build (2019-04-29) we have to install devtools and install readxl from master (https://github.com/tidyverse/readxl/releases)
RUN apk --no-cache add R R-doc R-dev libxml2-dev && \
    apk --no-cache add --virtual build-deps build-base linux-headers && \
    Rscript -e 'install.packages("devtools", repos="http://cran.us.r-project.org")' && \
    Rscript -e 'devtools::install_github("tidyverse/readxl")' && \
    Rscript -e 'install.packages("jsonlite", repos="http://cran.us.r-project.org")' && \
    Rscript -e 'install.packages("tidyverse", repos="http://cran.us.r-project.org")' && \
    apk --no-cache del --purge --rdepends build-deps

# Temporary build image
FROM base as build

WORKDIR /work
COPY . /work
RUN yarn install
RUN yarn compile

# Final small image
FROM base as final

ENV SERVER_PORT 3002
ENV SERVER_TIMEOUT 5000

WORKDIR /app
COPY --from=build /work/.build/ /app/.build/
COPY package.json /app
RUN yarn install --production

EXPOSE $SERVER_PORT

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
