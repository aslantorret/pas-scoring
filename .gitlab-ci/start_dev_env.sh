#!/bin/bash

PAS_CONTAINER_NAME=${CI_PROJECT_NAME}_${CI_COMMIT_REF_SLUG}_pas-scoring_1

if [[ -f deploy.${CI_COMMIT_REF_SLUG}.env ]]; then
    # Export on source
    set -a
    source deploy.${CI_COMMIT_REF_SLUG}.env
    echo "deploy.${CI_COMMIT_REF_SLUG}.env found with port number '$SERVER_PORT_EXTERNAL' configured."
    docker-compose --env-file .env.ci up --detach --remove-orphans --no-build
else
    echo "No deploy.env found. Creating new environment..."
    # 0 will give a random port
    export SERVER_PORT_EXTERNAL=0
    docker-compose --env-file .env.ci up --detach --remove-orphans --no-build

    PORT=$(docker port ${PAS_CONTAINER_NAME} | awk -F':' '{ print $NF}')
    echo "SERVER_PORT_EXTERNAL=$PORT" > deploy.${CI_COMMIT_REF_SLUG}.env
    echo "New deployment with port '$PORT' created."
fi

# Give it time to start/fail
sleep 2

if ! docker ps | grep ${PAS_CONTAINER_NAME} >/dev/null; then
    echo "Container is not running. Outputting logs:"
    docker-compose  --env-file .env.ci logs
    docker-compose  --env-file .env.ci down
    exit 1
fi
