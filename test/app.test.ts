import { Server } from 'http';
import * as url from 'url';
import axios from 'axios';

import app from '../src/app';

const port = app.get('port') || 8998;
const getUrl = (pathname?: string): string =>
  url.format({
    hostname: app.get('host') || 'localhost',
    protocol: 'http',
    port,
    pathname,
  });

describe('Feathers application tests (with jest)', () => {
  let server: Server;

  beforeAll((done) => {
    server = app.listen(port);
    server.once('listening', () => done());
  });

  afterAll((done) => {
    server.close(done);
  });

  describe('404', () => {
    it('return 404 json error response', async () => {
      expect.assertions(2);

      try {
        await axios.get(getUrl('path/to/nowhere'), {
          headers: {
            Accept: 'application/json',
          },
        });
      } catch (error) {
        const { response } = error;

        expect(response.status).toBe(404);
        expect(
          response.headers['content-type'].indexOf('application/json'),
        ).not.toBe(-1);
      }
    });
  });
});
