import { Application as ExpressFeathers } from '@feathersjs/express';
import { Params } from '@feathersjs/feathers';
import { Types } from 'mongoose';

export interface ServiceStabResult {
  [key: string]: string;
}
export interface ServiceTypes {}

export interface ServiceParams<T = {}> extends Params {
  query: T;
}

export type Application = ExpressFeathers<ServiceTypes> & {
  setup: (server?: Application) => Application;
};

export interface RefModel {
  _id: string | Types.ObjectId;
}
export interface ModelBase extends RefModel {
  createdAt: Date;
  updatedAt: Date;
  __v?: number;
}

export interface Hash<T> {
  [key: string]: T;
}
