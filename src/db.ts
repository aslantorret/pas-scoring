import mongoose from 'mongoose';
import { getConfig } from './configuration';

const { mongodb } = getConfig();

const {
  user,
  pass,
  host,
  port,
  database,
  poolSize,
  serverSelectionTimeout,
  replicaHosts,
} = mongodb;

let coma = '';
if (replicaHosts) {
  coma = ',';
}

const mongoUri = `mongodb://${user}:${pass}@${host}:${port}${coma}${replicaHosts}/${database}`;

mongoose.connect(mongoUri, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverSelectionTimeoutMS: serverSelectionTimeout,
  poolSize,
});

export const { connection } = mongoose;
export default mongoose;
