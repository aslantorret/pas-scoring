import app from './app';

const config = app.get('config');
const server = app.listen(config.port);
server.timeout = Number.parseInt(config.timeout, 10);
const logger = app.get('logger');

process.on('unhandledRejection', (reason: any) => {
  let text = reason;
  let extra: {} = {};
  if (reason instanceof Error && reason.message) {
    const { message, ...rest } = reason;
    text = message;
    extra = rest;
  }
  logger.error(`### Unhandled rejection: ${text}`, extra);
});

server.on('listening', () =>
  logger.info(
    'Feathers application started on http://%s:%d with timeout %d',
    config.host,
    config.port,
    config.timeout,
  ),
);
