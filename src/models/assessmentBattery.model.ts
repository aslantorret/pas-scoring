import { Connection, Document, Model, Mongoose, Types } from 'mongoose';
import { Application, ModelBase } from '../declarations';
import { User } from './user.model';
import { Assessment } from './assessment.model';

export interface AssessmentBattery extends ModelBase {
  user: User | Types.ObjectId;
  name: string;
  candidates: (User | Types.ObjectId)[];
  assessments: (Assessment | Types.ObjectId)[];
  openingDate: Date;
  expireDate: Date | null;
  exportableAssessments: number;
}

export interface AssessmentBatteriesDocument
  extends AssessmentBattery,
    Document {
  _id: Types.ObjectId;
  __v: number;
}

export default (app: Application): Model<AssessmentBatteriesDocument> => {
  const assessmentBatteryModelName = 'AssessmentBattery';
  const mongooseClient: Connection & Mongoose = app.get('mongooseClient');
  const assessmentBatterySchema = new mongooseClient.Schema(
    {
      user: { type: Types.ObjectId, ref: 'User', required: true },
      name: { type: String, unique: true, required: true },
      candidates: {
        type: [{ type: Types.ObjectId, ref: 'User' }],
        required: true,
      },
      assessments: {
        type: [{ type: Types.ObjectId, ref: 'Assessment' }],
        required: true,
      },
      openingDate: { type: Date, default: new Date() },
      expireDate: { type: Date, default: null },
      exportableAssessments: { type: Number, default: 0 },
    },
    { timestamps: true },
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(assessmentBatteryModelName)) {
    mongooseClient.deleteModel(assessmentBatteryModelName);
  }
  return mongooseClient.model<AssessmentBatteriesDocument>(
    assessmentBatteryModelName,
    assessmentBatterySchema,
    'assessmentBatteries',
  );
};
