import mongoose, { Document, Schema, SchemaOptions, Types } from 'mongoose';
import { ModelBase } from '../declarations';

export enum AssessmentQuestionTypes {
  radio = 'radio',
  dropDown = 'dropDown',
}

export type QuestionAnswer = {
  _id: Types.ObjectId;
  text: string;
  textArabic: string;
  index: number;
  default: boolean;
};

interface AssessmentQuestionBase extends ModelBase {
  description: string;
  descriptionArabic: string;
  index: number;
}

export interface AssessmentQuestionRadio extends AssessmentQuestionBase {
  typeCode: AssessmentQuestionTypes.radio;
  answers: QuestionAnswer[];
}

export interface AssessmentQuestionDropDown extends AssessmentQuestionBase {
  typeCode: AssessmentQuestionTypes.dropDown;
  answers: [
    {
      _id: Types.ObjectId;
      text: string;
      textArabic: string;
      index: number;
      default: boolean;
    },
  ];
  options: {
    placeholder: string;
  };
}

export type AssessmentQuestion =
  | AssessmentQuestionRadio
  | AssessmentQuestionDropDown;
export type AssessmentQuestionDocument = (
  | AssessmentQuestionRadio
  | AssessmentQuestionDropDown
) &
  Document;

const options: SchemaOptions = {
  discriminatorKey: 'typeCode',
  timestamps: true,
  strict: 'throw',
};

export const AssessmentQuestionBaseSchema = new Schema(
  {
    typeCode: {
      type: String,
      required: true,
      enum: [AssessmentQuestionTypes.radio, AssessmentQuestionTypes.dropDown],
    },
    description: { type: String, required: true },
    descriptionArabic: { type: String, required: true },
    status: String,
    index: { type: Number, required: true },
  },
  options,
);

export const AssessmentQuestionBaseModel = mongoose.model<
  AssessmentQuestionDocument
>('AssessmentQuestion', AssessmentQuestionBaseSchema);

export const AssessmentQuestionRadioModel = AssessmentQuestionBaseModel.discriminator<
  AssessmentQuestionRadio & Document
>(
  AssessmentQuestionTypes.radio,
  new Schema(
    {
      answers: {
        type: [
          {
            _id: Schema.Types.ObjectId,
            text: String,
            textArabic: String,
            index: Number,
          },
        ],
        required: true,
        default: undefined,
      },
      // options: { placeholder: { type: String, required: true } },
    },
    {
      ...options,
    },
  ),
);

export const AssessmentQuestionDropDownModel = AssessmentQuestionBaseModel.discriminator<
  AssessmentQuestionDropDown & Document
>(
  AssessmentQuestionTypes.dropDown,
  new Schema(
    {
      answers: {
        type: [
          {
            _id: Schema.Types.ObjectId,
            text: String,
            textArabic: String,
            value: Number,
            index: Number,
            default: Boolean,
          },
        ],
        required: true,
        default: undefined,
      },
      options: { placeholder: { type: String, required: true } },
    },
    {
      ...options,
    },
  ),
);
