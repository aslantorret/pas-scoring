import mongoose, { Document, Schema, SchemaOptions, Types } from 'mongoose';
import { ModelBase } from '../declarations';

export enum CandidateAnswerTypes {
  radio = 'radioAnswer',
  dropDown = 'dropDownAnswer',
  chxList = 'chxListAnswer',
}

interface CandidateAnswerBase extends ModelBase {
  questionId: Types.ObjectId;
}

export interface CandidateAnswerRadio extends CandidateAnswerBase {
  typeCode: CandidateAnswerTypes.radio;
  data: Types.ObjectId;
}

export interface CandidateAnswerDropDown extends CandidateAnswerBase {
  typeCode: CandidateAnswerTypes.dropDown;
  data: Types.ObjectId;
}

export interface CandidateAnswerChxList extends CandidateAnswerBase {
  typeCode: CandidateAnswerTypes.dropDown;
  data: Types.ObjectId[];
}

export type CandidateAnswer = CandidateAnswerRadio | CandidateAnswerDropDown;
export type CandidateAnswerDocument = (
  | CandidateAnswerRadio
  | CandidateAnswerDropDown
) &
  Document;

const options: SchemaOptions = {
  discriminatorKey: 'typeCode',
  timestamps: true,
  strict: 'throw',
};

export const CandidateAnswerBaseSchema = new Schema(
  {
    typeCode: {
      type: String,
      required: true,
      enum: [
        //
        CandidateAnswerTypes.radio,
        CandidateAnswerTypes.dropDown,
      ],
    },
    questionId: Schema.Types.ObjectId,
  },
  options,
);

export const CandidateAnswerBaseModel = mongoose.model<CandidateAnswerDocument>(
  'CandidateAnswer',
  CandidateAnswerBaseSchema,
);

export const CandidateAnswerRadioModel = CandidateAnswerBaseModel.discriminator<
  CandidateAnswerRadio & Document
>(
  CandidateAnswerTypes.radio,
  new Schema(
    {
      data: { type: Schema.Types.ObjectId, required: true },
    },
    options,
  ),
);

export const CandidateAnswerDropDownModel = CandidateAnswerBaseModel.discriminator<
  CandidateAnswerDropDown & Document
>(
  CandidateAnswerTypes.dropDown,
  new Schema(
    {
      data: { type: Schema.Types.ObjectId, required: true },
    },
    options,
  ),
);

export const CandidateAnswerChxListModel = CandidateAnswerBaseModel.discriminator<
  CandidateAnswerDropDown & Document
>(
  CandidateAnswerTypes.chxList,
  new Schema(
    {
      data: { type: [Schema.Types.ObjectId], required: true },
    },
    options,
  ),
);
