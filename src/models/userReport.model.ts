import { Connection, Document, Model, Mongoose, Schema, Types } from 'mongoose';
import { Application, ModelBase } from '../declarations';

import { AssessmentBattery } from './assessmentBattery.model';
import { User } from './user.model';
import { CandidateAssessment } from './candidateAssessment.model';

export interface UserReport extends ModelBase {
  user: User | Types.ObjectId;
  battery: AssessmentBattery | Types.ObjectId;
  candAssessments: (CandidateAssessment | Types.ObjectId)[];
}

export interface UserReportDocument extends UserReport, Document {
  _id: Types.ObjectId;
  __v: number;
}

export interface UserReportModel extends Model<UserReportDocument> {}

export default (app: Application): Model<UserReportDocument> => {
  const mongooseClient: Connection & Mongoose = app.get('mongooseClient');
  const userReportModelName = 'UserReport';

  const userReportSchema = new Schema(
    {
      user: { type: Types.ObjectId, ref: 'User', required: true },
      battery: {
        type: Types.ObjectId,
        ref: 'AssessmentBattery',
        required: true,
      },
      candAssessments: {
        type: [
          {
            type: Types.ObjectId,
            ref: 'CandidateAssessment',
          },
        ],
        required: true,
      },
    },
    { timestamps: true },
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(userReportModelName)) {
    mongooseClient.deleteModel(userReportModelName);
  }
  return mongooseClient.model<UserReportDocument>(
    userReportModelName,
    userReportSchema,
    'userReport',
  );
};
