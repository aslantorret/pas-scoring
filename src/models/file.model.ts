import { Connection, Document, Model, Mongoose, Schema, Types } from 'mongoose';
import { Application, ModelBase } from '../declarations';

export enum FileTypes {
  MarkdownImage = 'MarkdownImage',
  AnswerImage = 'AnswerImage',
  RScript = 'RScript',
  CSVLookUp = 'CSVLookUp',
  ReportTemplate = 'ReportTemplate',
  Report = 'Report',
}

export enum FileOwnerModelNames {
  Assessment = 'Assessment',
  AssessmentBattery = 'AssessmentBattery',
}

export interface File extends ModelBase {
  type: keyof typeof FileTypes;
  owner: Types.ObjectId;
  ownerModel: keyof typeof FileOwnerModelNames;
  fileName: string;
  alias: string | null;
  mimeType: string;
  size: number;
  payload: Buffer;
}

export interface FileDocument extends File, Document {
  _id: Types.ObjectId;
  __v: number;
}

export default (app: Application): Model<FileDocument> => {
  const modelName = 'File';
  const mongooseClient: Connection & Mongoose = app.get('mongooseClient');
  const schema = new Schema(
    {
      type: {
        type: String,
        required: true,
        enum: [
          FileTypes.MarkdownImage,
          FileTypes.AnswerImage,
          FileTypes.RScript,
          FileTypes.CSVLookUp,
          FileTypes.ReportTemplate,
          FileTypes.Report,
        ],
      },
      owner: {
        type: Schema.Types.ObjectId,
        required: true,
        refPath: 'ownerModel',
      },
      ownerModel: {
        type: String,
        required: true,
        enum: [
          FileOwnerModelNames.Assessment,
          FileOwnerModelNames.AssessmentBattery,
        ],
      },
      mimeType: { type: String, required: true },
      fileName: { type: String, required: true },
      alias: { type: String, default: null },
      size: { type: Number, required: true },
      payload: Buffer,
    },
    { timestamps: true },
  );

  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model<FileDocument>(modelName, schema);
};
