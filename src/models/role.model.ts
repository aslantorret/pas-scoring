import { Connection, Document, Model, Mongoose, Schema, Types } from 'mongoose';
import { Application, ModelBase } from '../declarations';

export enum RoleNames {
  Candidate = 'Candidate',
  Operator = 'Operator',
  Administrator = 'Administrator',
}

export interface Role extends ModelBase {
  name: keyof typeof RoleNames;
  permissions: string[];
}

export interface RoleDocument extends Role, Document {
  _id: Types.ObjectId;
  __v: number;
}

export default (app: Application): Model<RoleDocument> => {
  const modelName = 'Role';
  const mongooseClient: Connection & Mongoose = app.get('mongooseClient');
  const schema = new Schema(
    {
      name: {
        type: String,
        unique: true,
        required: true,
        enum: [
          RoleNames.Candidate,
          RoleNames.Operator,
          RoleNames.Administrator,
        ],
      },
      permissions: [String],
    },
    { timestamps: true },
  );

  // T his is necessary to avoid model compilation errors in watch mode
  // s ee https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model<RoleDocument>(modelName, schema);
};
