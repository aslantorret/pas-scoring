import { Connection, Document, Model, Mongoose, Schema, Types } from 'mongoose';
import { Application, ModelBase } from '../declarations';
import { Role } from './role.model';

export interface User extends ModelBase {
  // TODO: These fields will be split further
  //  to different interfaces
  password: string;
  userName: string;
  role: Role | Types.ObjectId;
  phone: string | null;
  email: string | null;
  fullName: string;
  gender: string;
  nationality: string | null;
  dateOfBirth: Date;
  acknowledgeParticipating: boolean;
  agreeToRelease: boolean;
  agreeToProcess: boolean;
}

export interface UserDocument extends User, Document {
  _id: Types.ObjectId;
  __v: number;
}

export default (app: Application): Model<UserDocument> => {
  const modelName = 'User';
  const mongooseClient: Connection & Mongoose = app.get('mongooseClient');
  const schema = new Schema(
    // TODO: This schema contains all necessary
    //  fields for user data on frontend.
    //  Adjust it further
    {
      userName: { type: String, unique: true, required: true },
      email: { type: String, lowercase: true, default: null },
      role: { type: Types.ObjectId, ref: 'Role', required: true },
      password: { type: String, required: true },
      phone: { type: String, default: null },
      fullName: { type: String, required: true },
      gender: { type: String, required: true },
      nationality: { type: String, default: null },
      dateOfBirth: { type: Date, required: true },
      acknowledgeParticipating: { type: Boolean, default: false },
      agreeToRelease: { type: Boolean, default: false },
      agreeToProcess: { type: Boolean, default: false },
    },
    { timestamps: true },
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model<UserDocument>(modelName, schema);
};
