import { Connection, Document, Model, Mongoose, Schema, Types } from 'mongoose';
import { Application, ModelBase } from '../declarations';
import { User } from './user.model';
import { Assessment } from './assessment.model';
import {
  CandidateAnswer,
  CandidateAnswerBaseModel,
} from './candidateAnswer.model';
import { AssessmentBattery } from './assessmentBattery.model';

export enum CandidateAssessmentStatuses {
  pending = 'pending',
  inProgress = 'inProgress',
  done = 'done',
  expired = 'expired',
  runOut = 'runOut',
  scoring = 'scoring',
  scored = 'scored',
  scoreFailed = 'scoreFailed',
}

export interface CandidateAssessment extends ModelBase {
  user: User | Types.ObjectId;
  assessment: Assessment | Types.ObjectId;
  status: CandidateAssessmentStatuses;
  startDate: Date | null;
  finishDate: Date | null;
  openingDate: Date;
  expireDate: Date | null;
  deadlineTimestamp: number | null;
  duration: Assessment['duration'];
  candidateAnswers: CandidateAnswer[];
  strictDeadline: Assessment['strictDeadline'];
  battery: AssessmentBattery | Types.ObjectId | null;
  optionalAnswers: Assessment['optionalAnswers'];
  language: string | null;
  reset: boolean;
  resetDates: Date[];
  scoringResult: Record<string, string> | null;
  errorMessage: string | null;
}

export type CandidateAssessmentCreate = Partial<CandidateAssessment> &
  Pick<CandidateAssessment, 'user' | 'assessment' | 'duration'>;

export interface CandidateAssessmentDocument
  extends CandidateAssessment,
    Document {
  _id: Types.ObjectId;
  __v: number;
}

export enum CandidateAssessmentLanguages {
  enGB = 'enGB',
  arEG = 'arEG',
}

export default (app: Application): Model<CandidateAssessmentDocument> => {
  const candidateAssessmentModelName = 'CandidateAssessment';
  const mongooseClient: Connection & Mongoose = app.get('mongooseClient');
  const candidateAssessmentSchema = new mongooseClient.Schema(
    {
      user: { type: Types.ObjectId, ref: 'User', required: true },
      assessment: { type: Types.ObjectId, ref: 'Assessment', required: true },
      battery: { type: Types.ObjectId, ref: 'AssessmentBattery' },
      status: {
        type: String,
        required: true,
        enum: [
          CandidateAssessmentStatuses.pending,
          CandidateAssessmentStatuses.inProgress,
          CandidateAssessmentStatuses.done,
          CandidateAssessmentStatuses.expired,
          CandidateAssessmentStatuses.runOut,
          CandidateAssessmentStatuses.scoring,
          CandidateAssessmentStatuses.scored,
          CandidateAssessmentStatuses.scoreFailed,
        ],
        default: CandidateAssessmentStatuses.pending,
      },
      startDate: { type: Date, default: null },
      finishDate: { type: Date, default: null },
      openingDate: { type: Date, default: new Date() },
      expireDate: { type: Date, default: null },
      duration: { type: Number, required: true },
      deadlineTimestamp: { type: Number, default: null },
      candidateAnswers: {
        type: [{ type: CandidateAnswerBaseModel.schema }],
        required: true,
        default: [],
      },
      strictDeadline: { type: Boolean, required: true },
      optionalAnswers: { type: Boolean, required: true },
      language: { type: String, default: null },
      reset: { type: Boolean, default: false },
      resetDates: {
        type: [{ type: Date }],
        required: true,
        default: [],
      },
      scoringResult: {
        type: Object,
        default: null,
      },
      errorMessage: {
        type: String,
        default: null,
      },
    },
    { timestamps: true },
  );

  const candidateAnswersDocumentArray = candidateAssessmentSchema.path(
    'candidateAnswers',
  ) as Schema.Types.DocumentArray;

  const { discriminators } = CandidateAnswerBaseModel;
  if (discriminators) {
    Object.keys(discriminators).forEach((key) => {
      const m = discriminators[key];
      candidateAnswersDocumentArray.discriminator(key, m.schema);
    });
  }

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(candidateAssessmentModelName)) {
    mongooseClient.deleteModel(candidateAssessmentModelName);
  }
  return mongooseClient.model<CandidateAssessmentDocument>(
    candidateAssessmentModelName,
    candidateAssessmentSchema,
    'candidateAssessments',
  );
};
