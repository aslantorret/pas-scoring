import { Connection, Document, Model, Mongoose, Schema, Types } from 'mongoose';
import { Application, ModelBase } from '../declarations';
import {
  AssessmentQuestion,
  AssessmentQuestionBaseModel,
} from './question.model';

export interface Assessment extends ModelBase {
  name: string;
  nameArabic: string;
  description: string;
  descriptionArabic: string;
  questions: AssessmentQuestion[];
  duration: number;
  strictDeadline: boolean;
  optionalAnswers: boolean;
  status: AssessmentStatuses;

  // @TODO: can not use "Types.ObjectId | File" due circular dependencies
  rscript: any | null;
  // @TODO: can not use "Types.ObjectId | File" due circular dependencies
  csvLookUps: any[] | [];
}

export interface AssessmentDocument extends Assessment, Document {
  _id: Types.ObjectId;
  __v: number;
}

export enum AssessmentStatuses {
  draft = 'draft',
  ready = 'ready',
  archived = 'archived',
}

export interface AssessmentModel extends Model<AssessmentDocument> {}

export default (app: Application): Model<AssessmentDocument> => {
  const mongoose: Connection & Mongoose = app.get('mongooseClient');
  const assessmentModelName = 'Assessment';

  const assessmentSchema = new Schema(
    {
      name: { type: String },
      nameArabic: { type: String },
      description: { type: String },
      descriptionArabic: { type: String },
      questions: {
        type: [AssessmentQuestionBaseModel.schema],
        required: true,
        default: [],
      },
      duration: { type: Number, required: true },
      strictDeadline: { type: Boolean, default: true },
      optionalAnswers: { type: Boolean, default: false },
      status: {
        type: String,
        required: true,
        enum: [
          AssessmentStatuses.draft,
          AssessmentStatuses.ready,
          AssessmentStatuses.archived,
        ],
        default: AssessmentStatuses.draft,
      },
      rscript: { type: Types.ObjectId, ref: 'File', default: null },
      csvLookUps: {
        type: [{ type: Types.ObjectId, ref: 'File' }],
        required: true,
        default: [],
      },
    },
    { timestamps: true },
  );

  const questionsDocumentArray = assessmentSchema.path(
    'questions',
  ) as Schema.Types.DocumentArray;

  const { discriminators } = AssessmentQuestionBaseModel;
  if (discriminators) {
    Object.keys(discriminators).forEach((key) => {
      const m = discriminators[key];
      questionsDocumentArray.discriminator(key, m.schema);
    });
  }

  if (mongoose.modelNames().includes(assessmentModelName)) {
    mongoose.deleteModel(assessmentModelName);
  }
  return mongoose.model<AssessmentDocument>(
    assessmentModelName,
    assessmentSchema,
  );
};
