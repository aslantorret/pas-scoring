/* eslint-disable import/newline-after-import,import/first */
// ### this code have to be on this line
import path from 'path';
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../config/');
// ###
import configuration from '@feathersjs/configuration';
import { Application } from './declarations';

export function getConfig() {
  return configuration()();
}
export default (app: Application): void => {
  app.set('config', getConfig());
};
