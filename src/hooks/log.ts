import errors from '@feathersjs/errors';

import { HookContext } from '@feathersjs/feathers';

export default function () {
  return (context: HookContext): void => {
    const logger = context.app.get('logger');
    const config = context.app.get('config');

    if (context.error) {
      if (context.error.type !== errors.FeathersError.name) {
        const newError = new errors.GeneralError(
          context.error.message,
          context.error.data,
        );
        newError.stack = context.error.stack;
        // newError.parent = context.error.parent || {};
        context.error = newError;
      }

      logger.error(
        `%s \n%s`,
        context.error.stack,
        JSON.stringify(context.error, null, 2),
      );

      let extErrorMsg = '';
      if (context.error.original) {
        extErrorMsg += ` >>> error.original:${context.error.original.message}`;
      }
      if (context.error.parent?.message) {
        extErrorMsg += ` >>> error.parent:${context.error.parent.message}`;
      }
      if (context.error.errors && context.error.errors.length) {
        extErrorMsg += ` >>> error.errors[0]:${context.error.errors[0].message}`;
      }
      if (extErrorMsg) {
        logger.error(context.error.message + extErrorMsg);
      }

      return;
    }

    if (
      (context.type === 'before' && config.logger.logBefore) ||
      (context.type === 'after' && config.logger.logAfter)
    ) {
      logger.debug('', { context });
    }
  };
}
