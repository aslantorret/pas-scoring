import { string as JoiString } from '@hapi/joi';
import { HookContext } from '@feathersjs/feathers';
import { CandidateAssessmentStatuses } from '../models/candidateAssessment.model';

export const JoiObjectId = JoiString().alphanum().length(24).required();
export const JoiOptions = {
  abortEarly: false,
  convert: false,
};

export function setScoreFailedStatus() {
  return async (context: HookContext) => {
    const { CandidateAssessment } = context.app.get('mongooseClient').models;
    await CandidateAssessment.updateOne(
      { _id: context.data.candAssessmentId },
      {
        $set: {
          status: CandidateAssessmentStatuses.scoreFailed,
          errorMessage: JSON.stringify(context.error),
        },
      },
    );
    return Promise.resolve(context);
  };
}
