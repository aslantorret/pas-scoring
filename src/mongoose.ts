import mongoose, { Mongoose } from 'mongoose';
import { Application } from './declarations';
import { getConfig } from './configuration';
import { logger } from './log';

import candidateAssessmentModel from './models/candidateAssessment.model';
import assessmentModel from './models/assessment.model';
import FileModel from './models/file.model';
// eslint-disable-next-line import/no-named-as-default
import UserReportModel from './models/userReport.model';
import UserModel from './models/user.model';
import AssessmentBatteryModel from './models/assessmentBattery.model';

const { mongodb: config } = getConfig();

export interface MongodbConfig {
  host: string;
  port: string;
  poolSize: number;
  serverSelectionTimeout: number;
  replicaHosts: string;
  database: string;
  user: string;
  pass: string;
}

export function initMongoose(mongoConfig: MongodbConfig = config): Mongoose {
  const {
    user,
    pass,
    host,
    database,
    poolSize,
    serverSelectionTimeout,
    replicaHosts,
  } = mongoConfig;

  let { port } = mongoConfig;

  let colon = ':';
  if (!Number.parseInt(port, 10)) {
    colon = '';
    port = '';
  }

  let coma = '';
  if (replicaHosts) {
    coma = ',';
  }

  const mongoUri = `mongodb://${user}:${pass}@${host}${colon}${port}${coma}${replicaHosts}/${database}`;
  logger.info(mongoUri.replace(/mongodb:\/\/.*@/, 'mongodb://***:***@'));

  mongoose.connect(mongoUri, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverSelectionTimeoutMS: serverSelectionTimeout,
    poolSize,
  });
  return mongoose;
}
export default (app: Application): void => {
  const mongooseClient = initMongoose(config);
  mongooseClient.Promise = global.Promise;

  const { connection } = mongooseClient;

  connection.on('error', (err) => logger.error(err));
  connection.on('connected', () =>
    logger.info(`Mongoose - connected to DB ${connection.name}`),
  );
  connection.on('reconnected', () => logger.info('Mongoose - reconnected'));
  connection.on('disconnected', () => logger.warn('Mongoose - disconnected'));

  app.get('connectionsPool')?.add(() => {
    return mongooseClient.disconnect();
  });

  app.set('mongooseClient', mongooseClient);
  app.configure(candidateAssessmentModel);
  app.configure(assessmentModel);
  app.configure(FileModel);
  app.configure(UserReportModel);
  app.configure(UserModel);
  app.configure(AssessmentBatteryModel);
};
