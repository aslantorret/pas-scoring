import { disallow } from 'feathers-hooks-common';
import validate from './scoring.validator';
import { setScoreFailedStatus } from '../../hooks/helper';

export default {
  before: {
    all: [],
    find: disallow(),
    get: disallow(),
    create: [
      //
      validate.create(),
    ],
    update: disallow(),
    patch: disallow(),
    remove: disallow(),
  },
  error: {
    create: setScoreFailedStatus(),
  },
};
