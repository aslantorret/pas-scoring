// Initializes the `health` service on path `/health`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import ScoringService, { ScoringServiceResult } from './scoring.class';
import hooks from './scoring.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    scoring: ScoringService & ServiceAddons<ScoringServiceResult>;
  }
}

export default function (app: Application) {
  const { candidateAnswers } = app.get('config');
  const options = {
    candidateAnswers,
  };

  // Initialize our service with any options it requires
  app.use('/scoring', new ScoringService(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('scoring');

  service.hooks(hooks);
}
