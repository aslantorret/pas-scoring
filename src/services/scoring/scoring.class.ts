import { ServiceMethods } from '@feathersjs/feathers';
import R from 'r-script';
import { promises as fs } from 'fs';
import csv from 'csvtojson';
import { BadRequest, GeneralError, NotFound } from '@feathersjs/errors';
import { Model, Types } from 'mongoose';
import { Application } from '../../declarations';
import {
  CandidateAssessment,
  CandidateAssessmentDocument,
  CandidateAssessmentStatuses,
} from '../../models/candidateAssessment.model';
import {
  Assessment,
  AssessmentDocument,
  AssessmentStatuses,
} from '../../models/assessment.model';
import {
  AssessmentQuestion,
  QuestionAnswer,
} from '../../models/question.model';
import { CandidateAnswer } from '../../models/candidateAnswer.model';
import { UserReportDocument } from '../../models/userReport.model';
import { File, FileDocument, FileTypes } from '../../models/file.model';

export interface CREATE {
  candAssessmentId: string;
}

export interface ScoringServiceResult {}

interface ServiceOptions {
  candidateAnswers: {
    notAnsweredText: string;
  };
}

export default class ScoringService
  implements Partial<ServiceMethods<ScoringServiceResult>> {
  options: ServiceOptions;

  app: Application;

  CandidateAssessmentModel: Model<CandidateAssessmentDocument>;

  AssessmentModel: Model<AssessmentDocument>;

  UserReportModel: Model<UserReportDocument>;

  FileModel: Model<FileDocument>;

  constructor(options: ServiceOptions, app: Application) {
    this.options = options;
    this.app = app;
    const mongooseClient = app.get('mongooseClient');
    this.CandidateAssessmentModel = mongooseClient.models.CandidateAssessment;
    this.AssessmentModel = mongooseClient.models.Assessment;
    this.FileModel = mongooseClient.models.File;
    this.UserReportModel = mongooseClient.models.UserReport;
  }

  async create(data: CREATE) {
    const candAssessment = await this.CandidateAssessmentModel.findOne({
      _id: data.candAssessmentId,
    })
      .populate('assessment')
      .lean<CandidateAssessment>()
      .select(['user', 'candidateAnswers', 'battery', 'assessment'])
      .exec();

    if (!candAssessment) {
      throw new NotFound('Candidate assessment not found', {
        id: data.candAssessmentId,
      });
    }
    const assessment = candAssessment.assessment as Assessment;

    if (assessment.status !== AssessmentStatuses.ready) {
      throw new BadRequest('Assessment is not published', {
        candAssessmentId: data.candAssessmentId,
        status: assessment.status,
      });
    }

    await this.CandidateAssessmentModel.updateOne(
      { _id: data.candAssessmentId },
      {
        $set: {
          status: CandidateAssessmentStatuses.scoring,
        },
      },
    );
    const { CSVLookupFiles, RScriptFile } = await this._getAssessmentFiles(
      assessment._id as Types.ObjectId,
    );

    const candAnswersLookupTable = this._getCandAnswersLookupTable(
      candAssessment,
    );

    let lookupHasTable = {};

    try {
      lookupHasTable = await this._getJSONFromCsv(CSVLookupFiles);
    } catch (error) {
      throw new BadRequest('CSV Lookup file parsing error', { error });
    }

    const scriptInputPayload: {
      raw: Record<string, string>;
    } = { raw: candAnswersLookupTable, ...lookupHasTable };

    const scriptPath = `/tmp/${data.candAssessmentId}.R`;

    try {
      await fs.writeFile(scriptPath, RScriptFile.payload);
    } catch (error) {
      throw new BadRequest('R script creation error', { error });
    }

    const jsonPath = `/tmp/${data.candAssessmentId}.json`;
    try {
      await fs.writeFile(jsonPath, JSON.stringify(scriptInputPayload));
    } catch (error) {
      throw new BadRequest('Json creation error', { error });
    }

    let scoringResult;
    try {
      scoringResult = R(scriptPath).data(jsonPath).callSync();
    } catch (error) {
      throw new BadRequest('R script execution error', { error });
    }

    if (!scoringResult?.length) {
      throw new GeneralError('R script execution result is incorrect', {
        scoringResult,
      });
    }

    await this.CandidateAssessmentModel.updateOne(
      {
        _id: data.candAssessmentId,
      },
      {
        $set: {
          status: CandidateAssessmentStatuses.scored,
          scoringResult: scoringResult[0],
          errorMessage: null,
        },
      },
    );
    this._updateUserReport(
      candAssessment.battery as Types.ObjectId,
      candAssessment.user as Types.ObjectId,
    );
    return { success: true };
  }

  protected async _getJSONFromCsv(CSVLookupFiles: File[]) {
    const JSONLookupFiles = await Promise.all(
      CSVLookupFiles.map((lookupFile) =>
        csv({
          trim: true,
          delimiter: ',',
          checkType: true,
        }).fromString(lookupFile.payload.toString()),
      ),
    );
    const lookupHashTable: any = {};
    JSONLookupFiles.forEach((file, index: number) => {
      lookupHashTable[CSVLookupFiles[index].alias as string] = file;
    });
    return lookupHashTable;
  }

  protected async _getAssessmentFiles(assessmentId: Types.ObjectId) {
    const files = await this.FileModel.find({
      owner: assessmentId,
    });

    if (!files?.length) {
      throw new NotFound('Files not found', {
        assessmentId,
      });
    }

    const CSVLookupFiles = files.filter(
      (file: File) => file.type === FileTypes.CSVLookUp,
    );

    const RScriptFile = files.find(
      (file: File) => file.type === FileTypes.RScript,
    );

    if (!RScriptFile) {
      throw new NotFound(
        'Could not find RScript file in Files for Assessment',
        {
          assessmentId,
        },
      );
    }

    return { CSVLookupFiles, RScriptFile };
  }

  protected _getCandAnswersHashTable(candidateAnswers: CandidateAnswer[]) {
    return candidateAnswers.reduce(
      (acc: Record<string, Types.ObjectId>, value) => {
        acc[value.questionId.toString()] = value.data;
        return acc;
      },
      {},
    );
  }

  protected _getCandAnswersLookupTable(candAssessment: CandidateAssessment) {
    const { assessment, candidateAnswers, _id, user } = candAssessment;

    if (!assessment) {
      throw new BadRequest(
        'Assessment related to candidate assessment not found',
        { candAssessmentId: _id },
      );
    }

    const candAnswersHashTable = this._getCandAnswersHashTable(
      candidateAnswers,
    );

    const candAnswersLookupTable: Record<string, string> = {
      ID: user.toString(),
    };

    if ('questions' in assessment) {
      assessment.questions.forEach(
        (question: AssessmentQuestion, index: number) => {
          const questionId = question._id.toString();
          const candAnswerId = candAnswersHashTable[questionId];
          const questionAnswer = question.answers.find(
            (answer) => answer._id.toString() === candAnswerId.toString(),
          );
          candAnswersLookupTable[`Q${index + 1}`] = this._getCandAnswerValue(
            questionAnswer,
          );
        },
      );
    }
    return candAnswersLookupTable;
  }

  protected _getCandAnswerValue(questionAnswer: QuestionAnswer | undefined) {
    const { notAnsweredText } = this.options.candidateAnswers;
    return questionAnswer
      ? `${questionAnswer.index + 1}. ${questionAnswer.text}`
      : notAnsweredText;
  }

  protected async _updateUserReport(
    battery: Types.ObjectId,
    user: Types.ObjectId,
  ) {
    const candAssessments = await this.CandidateAssessmentModel.find({
      battery,
      user,
    });
    if (!candAssessments) {
      return;
    }
    const notScoredCandAssessment = candAssessments.filter(
      (candAssessment) =>
        (candAssessment.assessment as Assessment).rscript &&
        candAssessment.status !== CandidateAssessmentStatuses.scored,
    );
    if (notScoredCandAssessment.length > 0) {
      return;
    }
    await this.UserReportModel.create([
      {
        battery,
        user,
        candAssessments: candAssessments.map(
          (candAssessment) => candAssessment._id,
        ),
      },
    ]);
  }
}
