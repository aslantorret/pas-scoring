import { object as JoiObject } from '@hapi/joi';
import validate from '@feathers-plus/validate-joi';
import { JoiObjectId, JoiOptions } from '../../hooks/helper';

const createSchema = JoiObject().keys({
  candAssessmentId: JoiObjectId,
});

export default {
  create: () => validate.form(createSchema, JoiOptions),
};
