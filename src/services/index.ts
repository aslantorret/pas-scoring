import { Application } from '../declarations';
import scoring from './scoring/scoring.service';

export default (app: Application): void => {
  app.configure(scoring);
};
