import helmet from 'helmet';
import cors from 'cors';

import feathers from '@feathersjs/feathers';
import express from '@feathersjs/express';

import { Application } from './declarations';
import configuration from './configuration';
import log from './log';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import mongoose from './mongoose';

const app: Application = express(feathers());

// Load app configuration
app.configure(configuration);
const config = app.get('config');

app.configure(log);

// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(express.json({ limit: config.jsonSizeLimit }));
app.use(express.urlencoded({ extended: true }));
// Host the public folder
// app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(express.rest());

app.configure(mongoose);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// express.static(path.join(__dirname, '../scripts'));

// Set up event channels (see channels.js)
// app.configure(channels);
// app.configure(swaggerModel);
// Configure a middleware for 404s and the error handler
app.use(express.notFound());
// app.use(function(req, res, next) {
//   res.status(404).send('Page Not Found!');
// });
app.use(
  express.errorHandler({
    html: false,
    logger: app.get('logger'),
  }),
);

app.hooks(appHooks);

export default app;
