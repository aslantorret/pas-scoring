import { createLogger, format, transports, Logger } from 'winston';
import { getConfig } from './configuration';
import { Application } from './declarations';

const config = getConfig().logger;
// Configure the Winston logger. For the complete documentation see https://github.com/winstonjs/winston
export const logger: Logger = createLogger({
  // To see more detailed errors, change this to 'debug'
  level: config.level,
  format: format.combine(format.splat(), format.colorize(), format.simple()),
  transports: [
    new transports.Console({
      silent: process.argv.some((s) => s.substr(-4) === 'jest'),
    }),
  ],
});

export default (app: Application): void => {
  app.set('logger', logger);
};
